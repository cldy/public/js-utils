import Color from 'color'

// Convert Color objects to string for CSS-in-JS usage
export const rgb: (color: Color) => string = (color: Color) => color.rgb().string()
export const hex: (color: Color) => string = (color: Color) => color.hex()
export const hsl: (color: Color) => string = (color: Color) => color.hsl().string()

// Apply effects on colors for interaction states
export const hover: (color: Color) => Color = (color: Color) => color.lighten(0.1)
export const active: (color: Color) => Color = (color: Color) => color.darken(0.1)
export const disabled: (color: Color) => Color = (color: Color) => color.darken(0.3)