export function money (amount: number, options: { currency?: string, long?: boolean } = {}): string {
    const currency = options.currency || 'EUR'
    const long = options.long || false
    const fractionDigits = amount % 1 === 0 && !long ? 0 : 2

    return new Intl.NumberFormat('en', {style: 'currency', currency: currency, minimumFractionDigits: fractionDigits}).format(amount)
}

export const $ = money

export function cents (amountInCents: number, options: { currency?: string, long?: boolean } = {}): string {
    return money(amountInCents / 100, options)
}

export const c = cents