# Cloudey's Javascript Utilities

A small package of various utilities which we use in our projects.
Includes Typescript signatures.

Includes:
- color: Various utilities for working with Color objects from the 'color' package.
- money: Two functions for formatting numbers as money.
- url: Utilities for URLs

Licensed under MIT.
Cloudey (R) is a registered trademark of Cloudey IT Ltd.